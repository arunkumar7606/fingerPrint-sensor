package com.arun.aashu.fingerprintsensor;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.security.KeyStore;

import javax.crypto.Cipher;

public class FingerPrintActivity extends AppCompatActivity {

    private KeyStore keyStore;

    // variable used for storing the key in the Android Keystore
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private TextView textView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_print);


        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        textView = findViewById(R.id.errorText);

        if (!fingerprintManager.isHardwareDetected()) {

            textView.setText("Your Device have not a FingerPrint Sensor");
        } else {

            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.USE_FINGERPRINT)
                    != PackageManager.PERMISSION_GRANTED) {

                textView.setText("FingerPrint Authentication permission not enable");
            } else {

                if (!fingerprintManager.hasEnrolledFingerprints()) {

                    textView.setText("Register at least one fingerprint");
                } else {
                    // Checks Wether
                    if (!keyguardManager.isKeyguardSecure()) {

                        textView.setText("Lock Screen Security not enabled in Settings");
                    } else {
                        generateKey();
                        if (cipherInit()) {

                            FingerprintManager.CryptoObject cryptoObject =
                                    new FingerprintManager.CryptoObject(cipher);
                            FingerprintHandler helper = new FingerprintHandler(this);

                        }
                    }

                }

            }
        }


    }


}
